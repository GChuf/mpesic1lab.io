import Card from './Card.js'


class Deck {

    constructor(numDecks = 1) {
        this.cards = this.initCards(numDecks);
        this.currCardIndex = 0; 
    }


    initCards(numDecks) {
        var cards = []

        for(let n = 0; n < numDecks; n++) {
            Card.suits.forEach(suit => {
                for(var rank in Card.ranks) {
                    cards.push(new Card(suit, rank, Card.ranks[rank]));
                }
            });
        }

        return cards;
    }

    printCards() {
        for(let i = 0; i < this.cards.length; i++) {
            console.log(this.cards[i].fileName);
        }
    }

    // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
    shuffle() {
        for(let i = this.cards.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let tmp = this.cards[i];
            this.cards[i] = this.cards[j];
            this.cards[j] = tmp;
        }
    }

    nextCard() {
        return this.cards[this.currCardIndex++];
    }

}


export default Deck;